import { shallowMount } from '@vue/test-utils';
import Counter from '@/components/Counter.vue';

describe('Counter.vue', () => {
  const wrapper = shallowMount(Counter, {});
  test('increments counter', () => {
    wrapper.vm.increment();
		wrapper.vm.increment();
		expect(wrapper.vm.value).toBe(2);
  });
  test ('decrements counter', () => {
    wrapper.vm.decrement();
		expect(wrapper.vm.value).toBe(1);
  });
});