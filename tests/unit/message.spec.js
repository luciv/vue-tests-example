import { shallowMount } from '@vue/test-utils';
import Message from '@/components/Message.vue';

describe('Message.vue', () => {
  const msg = 'new message';
  const wrapper = shallowMount(Message, {
      propsData: { msg },
  });
  test('renders props.msg when passed', () => {
    expect(wrapper.text()).toMatch(msg);
  });
  test ('clicks to hidden message', async () => {
    await wrapper
            .find('.msg')
            .trigger('click');
    expect(wrapper.vm.hiddenStatus).toBe(true);
  });
});
